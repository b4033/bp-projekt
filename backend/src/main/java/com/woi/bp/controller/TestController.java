package com.woi.bp.controller;

import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@NoArgsConstructor
public class TestController {
    @GetMapping("/test")
    public Map<String, String> test(){
        return Map.of("a", "1", "b", "2");
    }
}
