package com.woi.bp.configuration;

import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfiguration {
    @Bean
    public OpenApiCustomiser openApiCustomizer(){
        return openApi -> openApi.setInfo(new Info() {{
            setVersion("1.0.0");
            setTitle("Spring Boot OpenAPI definition");
            setDescription("This is the API definition of the application.");
        }});
    }
}
